﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Times
{
    public static partial class TimesExtensions
    {
        public static void TimesParallel(this int count, Action callback)
        {
            count.TimesParallel(_ => callback());
        }

        public static void TimesParallel(this int count, Action<int> callback)
        {
            Parallel.For(0, count, callback);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Times
{
    public static partial class TimesExtensions
    {
        public static async IAsyncEnumerable<T> TimesAsync<T>(this int count, Func<Task<T>> callback)
        {
            var tasks = Enumerable.Range(0, count).Select(_ => callback()).ToList();

            foreach (var task in tasks)
            {
                yield return await task;
            }
        }

        public static async IAsyncEnumerable<T> TimesAsync<T>(this int count, Func<int, Task<T>> callback)
        {
            var tasks = Enumerable.Range(0, count).Select(i => callback(i)).ToList();

            foreach (var task in tasks)
            {
                yield return await task;
            }
        }
    }
}

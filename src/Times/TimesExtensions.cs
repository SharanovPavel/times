﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Times
{
    public static partial class TimesExtensions
    {
        public static void Times(this int count, Action callback)
        {
            count.Times(_ => callback());
        }

        public static IEnumerable<T> Times<T>(this int count, Func<T> callback)
        {
            for (var i = 0; i < count; i++)
            {
                yield return callback();
            }
        }

        public static void Times(this int count, Action<int> callback)
        {
            for(var i = 0; i < count; i++)
            {
                callback(i);
            }
        }

        public static IEnumerable<T> Times<T>(this int count, Func<int, T> callback)
        {
            for (var i = 0; i < count; i++)
            {
                yield return callback(i);
            }
        }

        public static TAccumulate TimesAggregate<TInput, TAccumulate>(this int count, TAccumulate seed, Func<TInput> inputFactory, Func<TAccumulate, TInput, TAccumulate> accumulate)
        {
            return Enumerable.Range(0, count).Select(_ => inputFactory()).Aggregate(seed, accumulate);
        }

        // alias for TimesAggregate
        public static TOutput TimesFold<TInput, TOutput>(this int count, TOutput seed, Func<TInput> inputFactory, Func<TOutput, TInput, TOutput> accumulate)
        {
            return count.TimesAggregate(seed, inputFactory, accumulate);
        }
    }
}

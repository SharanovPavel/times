﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Times.Tests
{
    public class TimesExtensionsTests
    {
        [Theory]
        [InlineData(int.MinValue, 0)]
        [InlineData(int.MaxValue, int.MaxValue)]
        [InlineData(0, 0)]
        public void Times_Action_BorderValues(int count, int expectedInvokeCount)
        {
            var actualInvokeCount = 0;
            count.Times(() => { actualInvokeCount++; });
            actualInvokeCount.Should().Be(expectedInvokeCount);
        }

        [Theory]
        [InlineData(0, 1, 0, 0)]
        [InlineData(1, 1, 0, 1)]
        [InlineData(5, 1, 0, 5)]
        [InlineData(5, 2, 0, 10)]
        public void Times_Fold_SumNumbers(int count, int input, int seed, int expectedSum)
        {
            var actualSum = count.TimesAggregate(seed, () => input, (acc, val) => acc + val);
            actualSum.Should().Be(expectedSum);
        }

        [Theory]
        [InlineData(0, "hello", "")]
        [InlineData(1, "hello", "hello")]
        [InlineData(3, "z", "zzz")]
        [InlineData(1_000_000, "", "")]
        public void Times_Fold_ConcatString(int count, string input, string expectedString)
        {
            string.Intern(input);
            var actualString = count.TimesAggregate(new StringBuilder(), () => input, (acc, val) => acc.Append(val)).ToString();
            actualString.Should().Be(expectedString);
        }

        [Fact]
        public void Times_Func_GenerateGuids()
        {
            var guids = 1000.Times(() => Guid.NewGuid());
            guids.Should().OnlyContain(guid => guid != Guid.Empty);
        }

        [Fact]
        public void Times_Func_GenerateRandomIntegers()
        {
            var min = 0;
            var max = 1000;
            var values = 1000.Times(() => new Random().Next(min, max)).ToList();
            Parallel.ForEach(values, val => val.Should().BeInRange(min, max));
        }
    }
}

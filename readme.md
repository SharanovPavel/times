# Times

### Action

```
100.Times(() => Console.WriteLine("Hello Worlds"));
```

### Function

```
var greetings = 100.Times(() => "Hello World!"); // IEnumerable<string>
```

### Aggregation (Folding)

```
// You can also use TimesAggregate method, this is the same
var stringResult = 100.TimesFold(
    new StringBuilder(), 
    () => "Hello World!", 
    (builder, value) => builder.AppendLine(value)); // there will be 100 "Hello World" lines in the result
```

### Async

```
var responses = 1000.TimesAsync(async () => 
{
    var client = new HttpClient();
    return await client.GetAsync("http://google.com");
}); // IAsyncEnumerable<HttpResponseMessage>

await foreach(var response in responses)
{
    // do work with response
}
```

### Parallel

```
100.TimesParallel(() => { //do work });
// Alias for Parallel.For(0, 100, () => { // do work }
```